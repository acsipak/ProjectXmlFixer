ProjectXmlFixer
---------------

About
-----
ProjectXmlFixer is a simple one-class Java tool for mass modification of
Eclipse IDE’s project level configuration files. (The .project files located in
every project’s root directory, which are essentially XML files.)

It currently support two main funcions:
* Add/remove FindBugs plugin related nature and builder settings. This is
  useful if, for some reason, you are stuck with a plugin version earlier than
  3.x. In this case, there’s no workspace-level setting for automatic FindBugs
  runs on incremental builds, one can set this flag only on the project level.
  The add/remove FindBugs nature and builder feature offers a workaround for
  this problem.
* Clear leftover clutter created by the (now uninstalled) Google plugin. While
  the GWT (the main reason for installing Google plugin) is a real gem in our
  opinion, installing the Google Plugin for Eclipse tends to introduce more
  problems than it actually solves. Also, uninstalling the Google plugin often
  leaves some clutter in .project files. ProjectXmlFixer can remove these
  leftover configuration entries completely.

ProjectXmlFixer is open source and licensed under the Apache License, Version
2.0.

ProjectXmlFixer has no library dependencies and is written on top of core Java
libraries.

Known issues and limitations
----------------------------
* It is strongly recommended to back up your project settings before using
  ProjectXmlFixer preferably by putting all your .project files under version
  control. A decent version control system also lets you diff the changes
  ProjectXmlFixer made to the XML structure of your project settings, which is
  also a recommended best practice.
* ProjectXmlFixer is known to work with Eclipse 4.3 (Kepler) and FindBugs
  Eclipse Plugin 2.0.3. If you use different versions, YMMV.
* ProjectXmlFixer is known to merge empty XML tag pairs into single tags, i.e.
  <comment></comment> may become simply <comment/>. This does not change the
  semantics of the .project file, it only clutters up the file diffs a bit.

Usage
-----
If you want to use ProjectXmlFixer for any reason, it is safe to assume that
you are developing Java using the Eclipse IDE (+JDT). Since ProjectXmlFixer is
a simple one-class utility, we recommend to simply download the source, and
then compile and run it in Eclipse.

ProjectXmlFixer has a command line interface. Invalid or missing arguments will
write a syntax reference and a detailed error message to the console - if you
are not sure how to start, just give it a run without arguments and read the
help.