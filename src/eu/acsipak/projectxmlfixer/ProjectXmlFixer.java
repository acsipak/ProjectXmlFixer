/*
 * Copyright 2016 Csipak Attila
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.acsipak.projectxmlfixer;

import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.StringWriter;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

/**
 * Search Eclipse .projects files in an eclipse workspace and apply the following (optional)
 * operations on all of them:
 * <ol>
 * <li>
 * Remove leftover elements created by Google Plugin. (Useful after uninstalling Google Plugin.)
 * </li>
 * <li>
 * Remove/add the FindBugs nature and builder from/to Java projects.
 * </li>
 * </ol>
 * @author Csipak Attila
 */
public class ProjectXmlFixer {

  // node names
  private static final String NN_ARGUMENTS = "arguments";
  private static final String NN_BUILD_COMMAND = "buildCommand";
  private static final String NN_NAME = "name";
  private static final String NN_NATURE = "nature";

  // builder values
  private static final String BUILDER_FINDBUGS =
      "edu.umd.cs.findbugs.plugin.eclipse.findbugsBuilder";
  private static final Set<String> GWT_BUILDERS = new HashSet<String>();
  static {
    GWT_BUILDERS.add("com.google.appengine.eclipse.core.enhancerbuilder");
    GWT_BUILDERS.add("com.google.appengine.eclipse.core.projectValidator");
    GWT_BUILDERS.add("com.google.gwt.eclipse.core.gwtProjectValidator");
    GWT_BUILDERS.add("com.google.gdt.eclipse.core.webAppProjectValidator");
    GWT_BUILDERS.add("com.instantiations.designer.gwt.GWTBuilder");
    GWT_BUILDERS.add("com.swtdesigner.gwt.GWTBuilder");
  }

  // nature values
  private static final String NATURE_JAVA = "org.eclipse.jdt.core.javanature";
  private static final String NATURE_FINDBUGS = "edu.umd.cs.findbugs.plugin.eclipse.findbugsNature";
  private static final Set<String> GWT_NATURES = new HashSet<String>();
  static {
    GWT_NATURES.add("com.google.gwt.eclipse.core.gwtNature");
    GWT_NATURES.add("com.instantiations.designer.gwt.GWTNature");
    GWT_NATURES.add("com.swtdesigner.gwt.GWTNature");
  }

  private static File workspaceRoot;
  /** TRUE - add, FALSE - remove */
  private static Boolean findBugsFlag;
  private static boolean removeGwtSettings = false;
  private static int filesChanged = 0;

  /**
   * @see #ARGS_SYNTAX
   */
  public static void main(String[] args) {
    parseArgs(args);
    processProjectFiles();
  }

  private static void log(String msg) {
    StringBuilder sb = new StringBuilder();
    // indentation
    for (int i = 0; i < Thread.currentThread().getStackTrace().length; i++) {
      sb.append(" ");
    }
    sb.append(msg);
    System.out.println(sb);
  }

  private static final String ARGS_SYNTAX =
      "Arguments syntax:\n"
      + "\n"
      + "<rootDir> [fb=add|remove] [rmGWT=true|false]\n"
      + "\n"
      + "rootDir - Eclipse workspace root directory.\n"
      + "fb - add/remove FindBugs nature & builder to java nature projects (default: no action).\n"
      + "rmGWT - Remove GWT plugin related settings? (default: false).\n"
      + "\n"
      + "Example:\n"
      + "\n"
      + "d:\\workspace\\allianz rmGWT=true fb=add";

  private static void parseArgs(final String[] args) {
    log("Parsing command line arguments...");
    if (args.length < 1 || args[0] == null || args[0].isEmpty()) {
      throwArgsException("ERROR: no Eclipse workspace root directory specified in first argument.");
    }
    workspaceRoot = new File(args[0]);
    if (!workspaceRoot.exists() || !workspaceRoot.isDirectory()) {
      throwArgsException("ERROR: no directory exists on path specified in first argument.");
    }
    for (int i = 1; i < args.length; i++) {
      String a = args[i];
      if (a.startsWith("fb=")) {
        if (a.endsWith("add")) {
          findBugsFlag = Boolean.TRUE;
        } else if (a.endsWith("remove")) {
          findBugsFlag = Boolean.FALSE;
        } else {
          throwArgsException("ERROR: invalid value specified for fb argument.");
        }
      } else if (a.startsWith("rmGWT=")) {
        removeGwtSettings = Boolean.parseBoolean(a.substring(a.lastIndexOf("=") + 1));
      } else {
        throwArgsException("ERROR: invalid argument.");
      }
    }
  }

  private static void throwArgsException(final String errorMsg) {
    log(errorMsg);
    log(ARGS_SYNTAX);
    throw new IllegalArgumentException();
  }

  private static void processProjectFiles() {
    log("Enlisting project directories...");
    File[] projectDirs = workspaceRoot.listFiles(new FileFilter() {

      @Override
      public boolean accept(File pathname) {
        return pathname.isDirectory() && !pathname.getName().startsWith(".");
      }
    });
    if (projectDirs == null) {
      log("I/O error occured listing workspace directory.");
      return;
    }
    log("Processing .project files...");
    for (File pd : projectDirs) {
      File pf = new File(pd.getAbsolutePath() + "\\.project");
      if (pf.exists() && pf.isFile()) {
        log("Found project file: " + pf);
        processProjectFile(pf);
        // return; // uncomment for testing only the first .project file found in the workspace
      }
    }
    log("Finished processing. Number of .project files changed: " + filesChanged);
  }

  private static DocumentBuilder DOCUMENT_BUILDER = null;

  private static DocumentBuilder getDocumentBuilderInstance() throws ParserConfigurationException {
    if (DOCUMENT_BUILDER == null) {
      DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
      DOCUMENT_BUILDER = dbf.newDocumentBuilder();
    }
    return DOCUMENT_BUILDER;
  }

  private static Transformer TRANSFORMER = null;

  private static Transformer getTransformerInstance() throws TransformerConfigurationException {
    if (TRANSFORMER == null) {
      TransformerFactory tf = TransformerFactory.newInstance();
      TRANSFORMER = tf.newTransformer();
      TRANSFORMER.setOutputProperty(OutputKeys.INDENT, "yes");
    }
    return TRANSFORMER;
  }

  private static XPath XPATH = null;

  private static XPath getXPathInstance() {
    if (XPATH == null) {
      XPathFactory xpf = XPathFactory.newInstance();
      XPATH = xpf.newXPath();
      return XPATH;
    }
    return XPATH;
  }

  private static XPathExpression NATURES_XPATH_EXPRESSION = null;

  private static XPathExpression getNaturesXPathExpression() throws XPathExpressionException {
    if (NATURES_XPATH_EXPRESSION == null) {
      NATURES_XPATH_EXPRESSION = getXPathInstance().compile("/projectDescription/natures");
    }
    return NATURES_XPATH_EXPRESSION;
  }

  private static XPathExpression BUILD_SPEC_XPATH_EXPRESSION = null;

  private static XPathExpression getBuildSpecXPathExpression() throws XPathExpressionException {
    if (BUILD_SPEC_XPATH_EXPRESSION == null) {
      BUILD_SPEC_XPATH_EXPRESSION = getXPathInstance().compile("/projectDescription/buildSpec");
    }
    return BUILD_SPEC_XPATH_EXPRESSION;
  }

  private static boolean processedFileChanged = false;
  private static boolean processedFileHasJavaNature = false;
  private static boolean processedFileHasFindBugsNature = false;

  private static void processProjectFile(final File projectFile) {
    try {
      log("Parsing .projects XML...");
      Document doc = getDocumentBuilderInstance().parse(projectFile);

      processedFileChanged = false;
      processedFileHasJavaNature = false;
      processedFileHasFindBugsNature = false;

      processNatures(
          (Element) getNaturesXPathExpression().evaluate(doc, XPathConstants.NODE), doc);
      processBuildSpec(
          (Element) getBuildSpecXPathExpression().evaluate(doc, XPathConstants.NODE), doc);

      if (processedFileChanged) {
        log("Overwriting .projects XML...");

        StringWriter sw = new StringWriter();
        getTransformerInstance().transform(
            new DOMSource(doc),
            // new StreamResult(System.out) // for debugging
            new StreamResult(sw) // for production mode
            );
        // hack: insert newline after XML declaration, see:
        // https://stackoverflow.com/questions/24551962
        String outputXmlString = sw.toString().replaceFirst(Pattern.quote("?>"), "?>\n");
        sw.close();
        FileOutputStream fos = new FileOutputStream(projectFile);
        fos.write(outputXmlString.getBytes("UTF-8"));
        fos.flush();
        fos.close();

        filesChanged++;
      } else {
        log("Left .projects XML unchanged.");
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private static void processNatures(Element naturesElement, Document doc) throws DOMException {
    log("Processing natures...");
    List<Node> nodesToRemove = new LinkedList<Node>();
    Node nNode = naturesElement.getFirstChild();
    while (nNode != null) {
      if (nNode.getNodeType() == Node.ELEMENT_NODE) {
        String textContent = ((Element) nNode).getTextContent();
        if (removeGwtSettings && GWT_NATURES.contains(textContent)) {
          nodesToRemove.add(nNode);
          log("GWT nature found, marked as removable.");
        } else if (!processedFileHasJavaNature && NATURE_JAVA.equals(textContent)) {
          processedFileHasJavaNature = true;
          log("Java nature found.");
        } else if (findBugsFlag != null && NATURE_FINDBUGS.equals(textContent)) {
          if (findBugsFlag.booleanValue()) {
            processedFileHasFindBugsNature = true;
            log("FindBugs nature found, no need to add another.");
          } else {
            nodesToRemove.add(nNode);
            log("FindBugs nature found, marked as removable.");
          }
        }
      }
      nNode = nNode.getNextSibling();
    }
    removeNodes(naturesElement, nodesToRemove);
    if (processedFileHasJavaNature
        && !processedFileHasFindBugsNature
        && Boolean.TRUE.equals(findBugsFlag)
        ) {
      log("Adding FindBugs nature...");

      Text indentPreNature = doc.createTextNode("\t");
      naturesElement.appendChild(indentPreNature);

      Element findBugsNature = doc.createElement(NN_NATURE);
      findBugsNature.setTextContent(NATURE_FINDBUGS);
      naturesElement.appendChild(findBugsNature);

      Text indentPostNature = doc.createTextNode("\n\t");
      naturesElement.appendChild(indentPostNature);

      processedFileChanged = true;
    }
  }

  private static void processBuildSpec(Element buildSpecElement, Document doc) {
    log("Processing buildSpec...");
    boolean mustAddFindBugsBuilder =
        processedFileHasJavaNature && Boolean.TRUE.equals(findBugsFlag);
    List<Node> nodesToRemove = new LinkedList<Node>();
    Node bsNode = buildSpecElement.getFirstChild();
    while (bsNode != null) {
      if (bsNode.getNodeType() == Node.ELEMENT_NODE) {
        String buildCommandName = extractBuildCommandName((Element) bsNode);
        if (removeGwtSettings && GWT_BUILDERS.contains(buildCommandName)) {
          nodesToRemove.add(bsNode);
          log("GWT buildCommand found, marked as removable.");
        } else if (findBugsFlag != null && BUILDER_FINDBUGS.equals(buildCommandName)) {
          if (findBugsFlag.booleanValue()) { // add
            // FindBugs builder already present, no need to add.
            mustAddFindBugsBuilder = false;
            log("FindBugs buildCommand found, no need to add another.");
          } else { // remove
            nodesToRemove.add(bsNode);
            log("FindBugs buildCommand found, marked as removable.");
          }
        }
      }
      bsNode = bsNode.getNextSibling();
    }
    removeNodes(buildSpecElement, nodesToRemove);
    if (mustAddFindBugsBuilder) {
      log("Adding FindBugs buildCommand...");
      Element findBugsBuildCommand = doc.createElement(NN_BUILD_COMMAND);

      Text indentPreName = doc.createTextNode("\n\t\t\t");
      findBugsBuildCommand.appendChild(indentPreName);
      Element name = doc.createElement(NN_NAME);
      name.setTextContent(BUILDER_FINDBUGS);
      findBugsBuildCommand.appendChild(name);

      Text indentPreArguments = doc.createTextNode("\n\t\t\t");
      findBugsBuildCommand.appendChild(indentPreArguments);
      Element arguments = doc.createElement(NN_ARGUMENTS);
      findBugsBuildCommand.appendChild(arguments);
      Text indentPostArguments = doc.createTextNode("\n\t\t");
      findBugsBuildCommand.appendChild(indentPostArguments);

      Text indentPreBuildCommand = doc.createTextNode("\t");
      buildSpecElement.appendChild(indentPreBuildCommand);
      buildSpecElement.appendChild(findBugsBuildCommand);
      Text indentPostBuildCommand = doc.createTextNode("\n\t");
      buildSpecElement.appendChild(indentPostBuildCommand);

      processedFileChanged = true;
    }
  }

  private static void removeNodes(Element rootElement, List<Node> nodesToRemove) {
    if (!nodesToRemove.isEmpty()) {
      processedFileChanged = true;
      for (Node n : nodesToRemove) {
        Node prevNode = n.getPreviousSibling();
        if (prevNode.getNodeType() == Node.TEXT_NODE
            && ((Text) prevNode).getNodeValue().trim().isEmpty()) {
          // remove preceding indentation & whitespace
          rootElement.removeChild(prevNode);
        }
        rootElement.removeChild(n);
      }
    }
  }

  private static String extractBuildCommandName(Element bsElement) {
    Node bcNode = bsElement.getFirstChild();
    while (bcNode != null) {
      if (bcNode.getNodeType() == Node.ELEMENT_NODE
          && NN_NAME.equals(((Element) bcNode).getNodeName())) {
        return ((Element) bcNode).getTextContent();
      }
      bcNode = bcNode.getNextSibling();
    }
    return null;
  }
}
